import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Spinner, Post, Modal } from 'src/components/common/common';

const EditPost = ({
  sharePost
}) => {
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.expandedPost
  }));

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handleExpandedPostEdit = React.useCallback(id => (
    dispatch(threadActionCreator.editExpandedPost(id))
  ), [dispatch]);

  const handleExpandedPostClose = () => handleExpandedPostToggle();

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={handleExpandedPostClose}
    >
      {post ? (
        <Modal.Content>
          <Post
            post={post}
            onExpandedPostToggle={handleExpandedPostToggle}
            onExpandedPostEdit={handleExpandedPostEdit}
            sharePost={sharePost}
          />
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

EditPost.propTypes = {
  sharePost: PropTypes.func.isRequired
};

export default EditPost;
